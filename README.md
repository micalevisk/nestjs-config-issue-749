```bash
npm ci && npm run build
```

you will see

```
src/main.ts:11:14 - error TS2769: No overload matches this call.
  Overload 1 of 4, '(propertyPath: never, options: ConfigGetOptions): undefined', gave the following error.
    Argument of type 'string' is not assignable to parameter of type 'never'.
  Overload 2 of 4, '(propertyPath: never, defaultValue: any): any', gave the following error.
    Argument of type 'string' is not assignable to parameter of type 'never'.

11   config.get('foo', 333)
                ~~~~~

src/main.ts:20:3 - error TS2769: No overload matches this call.
  Overload 1 of 4, '(propertyPath: any, options: ConfigGetOptions): any', gave the following error.
    Argument of type 'number' is not assignable to parameter of type 'ConfigGetOptions'.
  Overload 2 of 4, '(propertyPath: never, defaultValue: number): number', gave the following error.
    Argument of type 'string' is not assignable to parameter of type 'never'.

20   config.get<number>('foo', 333)
     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Found 2 error(s).
```
