import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import type { IConfig, IDatabaseConfig } from './config/configuration';

@Injectable()
export class AppService {
  constructor(private readonly configService: ConfigService<IConfig, true>) {}

  getFoo(): string {
    return this.configService.get('foo');
  }

  getNestedPropFromConfig(): number {
    return this.configService.get('database.port', { infer: true });
  }

  trasverseNestedPropFromConfig(): number {
    const dbConfig = this.configService.get<IDatabaseConfig>('database');
    return dbConfig.port;
  }
}
