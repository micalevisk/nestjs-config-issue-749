import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService)
  //    ^? ConfigService<any, any>                      in @nestjs/common v8.2.2
  //    ^? ConfigService<unknown, boolean>              in @nestjs/common v8.2.3

  config.get('foo', 333)
  /*
  No overload matches this call.
    Overload 1 of 4, '(propertyPath: never, options: ConfigGetOptions): undefined', gave the following error.
      Argument of type 'string' is not assignable to parameter of type 'never'.
    Overload 2 of 4, '(propertyPath: never, defaultValue: any): any', gave the following error.
      Argument of type 'string' is not assignable to parameter of type 'never'.ts(2769)
  */

  config.get<number>('foo', 333)
  /*
  No overload matches this call.
  Overload 1 of 4, '(propertyPath: any, options: ConfigGetOptions): any', gave the following error.
    Argument of type 'number' is not assignable to parameter of type 'ConfigGetOptions'.
  Overload 2 of 4, '(propertyPath: never, defaultValue: number): number', gave the following error.
    Argument of type 'string' is not assignable to parameter of type 'never'.ts(2769)
  */
}
bootstrap();
