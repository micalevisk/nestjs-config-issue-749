import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): any {
    return {
      databasePort: this.appService.getNestedPropFromConfig(),
      foo: this.appService.getFoo(),
      dbConfigPort: this.appService.trasverseNestedPropFromConfig(),
    }
  }
}
